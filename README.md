# README #

Ultra(hyper?)-light python-based FITS file viewer to liberate you from having to start DS9 to just glance at an image.

Usage:

viewfits example.fits

Features:


* Shows you a fits file


* Takes only the last two dimensions (e.g. if you have a radio image with STOKES and FREQ axes)

Vague ideas for the future:

* A '-log' switch


* Show only specified region/percentage of image


* Show only part of image which will take up a given percentage of my available RAM


* Show me all the images in a cube


* Axis labels