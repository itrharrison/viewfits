#!/usr/bin/env python

import sys
import numpy as np
from matplotlib import pyplot as plt
from astropy.io import fits

fname = sys.argv[1]
image = fits.getdata(fname)
ndim = len(image.shape)

if ndim > 2:
  unwanted_dims = tuple(-1*np.ones(ndim-2, dtype=int))
  image = image[unwanted_dims]

plt.imshow(image, cmap='plasma')
plt.show()
